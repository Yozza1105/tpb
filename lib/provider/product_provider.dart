import 'package:flutter/foundation.dart';
import 'package:shop_app/models/product.dart';

class ProductProvider with ChangeNotifier {
  final List<Product> _items = [
    Product(
      id: "p1",
      productName: "AMD Ryzen 5",
      oldPrice: 1399.99,
      price: 1299.99,
      imageUrl: "assets/images/a.jfif",
    ),
    Product(
      id: "p2",
      productName: "Keyboard Steelseries",
      oldPrice: 24899,
      price: 22500,
      imageUrl: "assets/images/b.jfif",
    ),
    Product(
      id: "p3",
      productName: "Mouse Logitech",
      oldPrice: 1200,
      price: 950,
      imageUrl: "assets/images/c.jpg",
    ),
    Product(
      id: "p4",
      productName: "Motherboard ASUS ROG",
      oldPrice: 700,
      price: 629,
      imageUrl: "assets/images/d.jpg",
    ),
    Product(
      id: "p5",
      productName: "Headset Logitech",
      oldPrice: 300,
      price: 199.99,
      imageUrl: "assets/images/e.jfif",
    ),
    Product(
      id: "p6",
      productName: "Monitor Samsung",
      oldPrice: 230,
      price: 299.99,
      imageUrl: "assets/images/f.jpg",
    ),
  ];

  List<Product> get items {
    return [..._items];
  }

  Product findById(String id) {
    return _items.firstWhere((prod) {
      return prod.id == id;
    });
  }

  void addProduct(Product newProduct) {
    _items.insert(0, newProduct);
    notifyListeners();
  }

  List<Product> get selectedFavorite {
    return _items.where((favProducts) {
      return favProducts.isFavorite;
    }).toList();
  }
}
